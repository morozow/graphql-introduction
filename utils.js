/*
* GraphQL Introduction
* Raman Marozau, 2018
*/

function log(name = '', ...args) {
  console.log(name, ...args);
}

function logEmptyLine() {
  log();
}

module.exports = {
  log,
  logEmptyLine,
};