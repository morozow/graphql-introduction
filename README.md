# GraphQL Introduction
GraphQL implementation with basic ```graphql-js```, JavaScript reference implementation for GraphQL,.

### Additional Information
1. Установка Yarn: https://yarnpkg.com/en/docs/install
2. Желательно:
    - Работа с любой git веткой:
        - UNIX: chmod +x ./install.sh && ./install.sh
        - Windows: запуск ./install.sh
3. Команды управления:
    - Branches:
        - master
        - intro
        - step-1
        - step-2
        - step-3
        - step-3.1
            - node server.js
        - step-4
        - step-5
            - yarn start
4. Дополнение
    - step-4
        - Тестирование происходит переходом в браузере в «/graphql?query={SOURCE}», где SOURCE - GraphQL запрос, согласно Схеме. 
            - graphqlArguments.schema
    - step-2
        - Invalid. Эквивалентен оценке 2 - все ок.