#!/bin/bash

rm -rf ./node_modules
rm -rf ./graphql-js && git submodule add -f https://github.com/graphql/graphql-js.git
cd graphql-js && yarn install && yarn run build && cd ..