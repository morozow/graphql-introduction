/*
* GraphQL Introduction
* Raman Marozau, 2018
*/

const { graphql, buildSchema } = require('./graphql-js/dist');
const { log, logEmptyLine } = require('./utils');

logEmptyLine();

// Construct the Schema, using GraphQL query language
const schema = buildSchema(`
  type Query {
    hello: String
  }
`);

// Request
const source = '{ hello }';

// The root provides a resolver function for each Schema query
const rootValue = {
  hello: () => {
    return 'Hello world!';
  },
};


/**
 * This is the primary entry point function for fulfilling GraphQL operations
 * by parsing, validating, and executing a GraphQL document along side a
 * GraphQL schema.
 *
 * More sophisticated GraphQL servers, such as those which persist queries,
 * may wish to separate the validation and execution phases to a static time
 * tooling step, and a server runtime step.
 *
 * Accepts either an object with named arguments, or individual arguments:
 *
 * schema:
 *    The GraphQL type system to use when validating and executing a query.
 * source:
 *    A GraphQL language formatted string representing the requested operation.
 * rootValue:
 *    The value provided as the first argument to resolver functions on the top
 *    level type (e.g. the query object type).
 */
graphql(schema, source, rootValue).then((response) => {
  log('Response: ', response);
  logEmptyLine();
});



